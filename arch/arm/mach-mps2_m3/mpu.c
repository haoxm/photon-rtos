/*
 * PhotonRTOS础光实时操作系统 -- mpu驱动
 *
 * Copyright (C) 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Jiayuan Liang <liangjiayuan@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */


#include <photon/sched.h>
#include <photon/sections.h>
#include <asm/mpu.h>

#define FIRST_ADDR                  0x00000000
#define LAST_ADDR                   0xFFFFFFFF

void enable_memory_protection()
{

}

/* Function that defines a data protection range in the corresponding CPU Data Protection Range Register (DPR).
 * Data protection ranges have 8-byte granularity.
 * As a result, the lower 3 bits of any address passed to the define_data_protection_range function will be discarded.
 * After enabling the Memory Protection, access to an address 'x' will be allowed only if:
 * lowerBoundAddress <= x < upperBoundAddress
 */
void define_data_protection_range(uint32 lowerBoundAddress, uint32 upperBoundAddress, uint8 range)
{

}

/* Function that defines a code protection range in the corresponding CPU Code Protection Range Register (CPR).
 * Code protection ranges have 8-byte granularity.
 * As a result, the lower 3 bits of any address passed to the define_code_protection_range function will be discarded.
 * After enabling the Memory Protection, access to an address 'x' will be allowed only if:
 * lowerBoundAddress <= x < upperBoundAddress
 */
void define_code_protection_range(uint32 lowerBoundAddress, uint32 upperBoundAddress, uint8 range)
{

}

/* Function to enable the data read access to a predefined Range in a Protection Set */
void enable_data_read(uint8 protectionSet, uint8 range)
{

}

/* Function to enable the data write access to a predefined Range in a Protection Set */
void enable_data_write(uint8 protectionSet, uint8 range)
{

}

/* Function to enable code execution access to a predefined Range in a Protection Set */
void enable_code_execution(uint8 protectionSet, uint8 range)
{

}

void arch_switch_mpu(void)
{

}


void mpu_init(void)
{

}
