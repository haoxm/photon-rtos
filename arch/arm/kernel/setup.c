/*
 * PhotonRTOS础光实时操作系统 -- 架构相关的启动过程
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/init.h>
#include <photon/irq.h>
#include <photon/smp_lock.h>
#include <photon/console.h>
#include <photon/init_integrity.h>
#include <asm/arm_arch_timer.h>

#include <asm/irqflags.h>
#include <asm/processor.h>
#include <asm/internel.h>

void init_IRQ(void)
{
	MODULE_INIT(init_IRQ);
	init_irq_controller();
	init_interrupt();
}

void init_time(void)
{
	MODULE_INIT(init_time);
	init_time_arch();
}

void start_arch(void)
{
	MODULE_INIT(start_arch);
	boot_state = EARLY_INIT;

	init_simple_console();
}
