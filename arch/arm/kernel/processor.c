/*
 * PhotonRTOS础光实时操作系统 -- 处理器相关
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <asm/asm-offsets.h>

void cpu_do_idle(void);

void cpu_do_idle(void)
{
	asm volatile(
		"dsb sy \n\t"
		"wfi \n\t"
	);
	return;
}
