/*
 * PhotonRTOS础光实时操作系统 -- 计算数据结构偏移，用于汇编代码
 *
 * Copyright (C) 2022, 2023, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/sched.h>
#include <photon/kbuild.h>
#include <photon/irq.h>
#include <photon/irqflags.h>

/**
 * 生成偏移的主函数，不和应用主函数冲突
 */
int32_t arm_asm_offsets(void);
int32_t arm_asm_offsets(void)
{
	BLANK();
	BLANK();

	DEFINE(TI_PREEMPT,		offsetof(struct process_desc, preempt_count));
	DEFINE(TI_FLAGS,		offsetof(struct process_desc, flags));

	DEFINE(THREAD_CPU_CONTEXT,	offsetof(struct task_desc, task_spot.cpu_context));

	return 0;
}
