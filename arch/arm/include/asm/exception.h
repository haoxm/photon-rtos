/*
 * PhotonRTOS础光实时操作系统 -- 异常处理头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_EXCEPTION_H
#define __ASM_EXCEPTION_H

#include <photon/types.h>

#ifndef __ASSEMBLY__

struct exception_spot {
	uint32_t regs[16];
	uint32_t sp;
	uint32_t pc;
	uint32_t pstate;
	uint32_t orig_x0;
	uint32_t syscallno;
};

#define __exception	__attribute__((section(".exception.text")))
#define __exception_irq_entry	__exception

asmlinkage void hung(uintptr_t addr, unsigned esr);

#endif /* __ASSEMBLY__ */

#endif	/* __ASM_EXCEPTION_H */
