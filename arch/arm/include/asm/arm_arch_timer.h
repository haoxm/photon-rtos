/*
 * PhotonRTOS础光实时操作系统 -- 体系架构定时器
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __CLKSOURCE_ARM_ARCH_TIMER_H
#define __CLKSOURCE_ARM_ARCH_TIMER_H

#include <asm/types.h>

/**
 * timer API
 *
 */
uint32_t arch_timer_get_rate(void);
void init_time_arch(void);
void init_timer_arch(void);
int xilinx_ttc_int_status(int cpu, uint32_t countid);

#endif
