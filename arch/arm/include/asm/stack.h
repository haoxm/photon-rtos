/*
 * PhotonRTOS础光实时操作系统 -- 栈相关
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */
#ifndef __ASM_STACK_H
#define __ASM_STACK_H

#define PROCESS_STACK_SIZE 8192

#define THREAD_START_SP PROCESS_STACK_SIZE - 16

#endif
