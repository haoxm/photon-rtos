/*
 * PhotonRTOS础光实时操作系统 -- 位操作头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_BITOPS_H
#define __ASM_BITOPS_H

#include <photon/bitops/builtin-__ffs.h>

#include <photon/bitops/ffz.h>

#ifndef _OS_BITOPS_H
#error only <photon/bitops.h> can be included directly
#endif

#include <asm/barrier.h>

/**
 * 系统定义
 *
 */
#define BITS_PER_LONG 32

#ifndef BITS_PER_LONG_LONG
#define BITS_PER_LONG_LONG 64
#endif

/**
 * 原子位操作
 *
 */
extern void atomic_set_bit(uint32_t nr, volatile uintptr_t *p);
extern void atomic_clear_bit(uint32_t nr, volatile uintptr_t *p);
#define ARCH_FUN_atomic_set_bit_DEFINED
#define ARCH_FUN_atomic_clear_bit_DEFINED

#include <photon/bitops/find.h>

#endif /* __ASM_BITOPS_H */
