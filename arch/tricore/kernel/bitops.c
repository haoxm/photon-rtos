/*
 * PhotonRTOS础光实时操作系统 -- 位运算
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Jiayuan Liang <liangjiayuan@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include "IfxCpu_IntrinsicsGnuc.h"
#include <asm/types.h>
void atomic_set_bit(uint32_t nr, volatile uintptr_t *p)
{
    Ifx__imaskldmst(p, 1, nr, 1);
}

void atomic_clear_bit(uint32_t nr, volatile uintptr_t *p)
{
	Ifx__imaskldmst(p, 0, nr, 1);
}

void atomic_change_bit(uint32_t nr, volatile uintptr_t *p)
{
	Ifx__imaskldmst(p, 1, nr, 1);
}
