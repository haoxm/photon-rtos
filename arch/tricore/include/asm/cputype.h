/*
 * PhotonRTOS础光实时操作系统 -- CPU头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_CPUTYPE_H
#define __ASM_CPUTYPE_H

#ifndef __ASSEMBLY__
static inline uint32_t __attribute_const__ read_cpuid_cachetype(void)
{

}

#endif

#endif
