/*
 * PhotonRTOS础光实时操作系统 -- 线程描述符
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_THREAD_INFO_H
#define __ASM_THREAD_INFO_H

#include <asm/types.h>
#include <asm/stack.h>

#ifndef __ASSEMBLY__


struct process_desc;

#define ARCH_FUN_current_proc_info_DEFINED
static  struct process_desc *current_proc_info(void)
{
	uintptr_t sp;
	__asm__ volatile (" mov.d\t %0, %%a10" :"=d"(sp) ::"memory");
	return (struct process_desc *)(sp & ~(PROCESS_STACK_SIZE - 1));
}

struct arch_process_desc {
};


#define thread_saved_fp(tsk)	\
	((uintptr_t)(tsk->task_spot.cpu_context.fp))

#endif /* __ASSEMBLY__ */
#endif /* __ASM_THREAD_INFO_H */
