/*
 * PhotonRTOS础光实时操作系统 -- IO端口读写头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_IO_H
#define __ASM_IO_H

#ifdef __KERNEL__

#include <asm/types.h>

#define __iormb()		rmb()
#define __iowmb()		wmb()

#define mmiowb()		do { } while (false)


static inline uint32_t __raw_readl(const volatile uintptr_t addr)
{
	uint32_t val;
	asm volatile("ld.w %r0, [%1]": "=r" (val) : "r" (addr));
	return val;
}



static inline void __raw_writel(uint32_t val, volatile uintptr_t addr)
{
	asm volatile("st.w %r0, [%1]" : : "r" (val), "r" (addr));
}


/**
 * readl_relaxed
 *
 */
#define readl_relaxed(c) ({ uint32_t _tmp__r = (( uint32_t)	\
					__raw_readl(c)); _tmp__r; })


/**
 * writel_relaxed
 *
 */
#define writel_relaxed(v,c)	__raw_writel(( uint32_t) (v),c)

/**
 * readl
 *
 */
#define readl(c)		({ uint32_t _tmp__v = readl_relaxed(c); __iormb(); _tmp__v; })
/**
 * writel
 *
 */
#define writel(v,c)		({ __iowmb(); writel_relaxed(v,c); })

#endif	/* __KERNEL__ */
#endif	/* __ASM_IO_H */
