/*
 * led.h
 *
 *  Created on: 2023年3月25日
 *      Author: kernelsoft
 */

#ifndef __ASM_ARM_LED_H
#define __ASM_ARM_LED_H

extern void initLED(void);

extern void blinkLED(void);

extern void blinkLED2(void);

extern void blinkLED3(void);

extern void blinkLED4(void);

#endif /* __ASM_ARM_LED_H */
