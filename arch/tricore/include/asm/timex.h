/*
 * PhotonRTOS础光实时操作系统 -- 时间相关头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_TIMEX_H
#define __ASM_TIMEX_H
#include <Std/IfxStm.h>
#include "arch_timer.h"

#define get_cycles() arch_get_cycles()
#define ARCH_FUN_get_cycles_DEFINED
#endif
