/*
 * 础光实时操作系统PhotonRTOS -- AUTOSAR事件实现
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */
#include <autosar/error.h>

VAR(int8, AUTOMATIC) string_of_status[][32] = {
	"E_OK",
	"E_OS_ACCESS",
	"E_OS_CALLEVEL",
	"E_OS_ID",
	"E_OS_LIMIT",
	"E_OS_NOFUNC",
	"E_OS_RESOURCE",
	"E_OS_STATE",
	"E_OS_VALUE",
	"E_OS_SERVICEID",
	"E_OS_ILLEGAL_ADDRESS",
	"E_OS_MISSINGEND",
	"E_OS_DISABLEDINT",
	"E_OS_STACKFAULT",
	"E_OS_PARAM_POINTER",
	"E_OS_PROTECTION_MEMORY",
	"E_OS_PROTECTION_TIME",
	"E_OS_PROTECTION_ARRIVAL",
	"E_OS_PROTECTION_LOCKED",
	"E_OS_PROTECTION_EXCEPTION",
	"E_OS_SPINLOCK",
	"E_OS_INTERFERENCE_DEADLOCK",
	"E_OS_NESTING_DEADLOCK",
	"E_OS_CORE",
};

FUNC(char *, OS_CODE) Status2String(VAR(StatusType, AUTOMATIC) status)
{
    return string_of_status[status];
}