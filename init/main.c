/*
 * 础光实时操作系统PhotonRTOS -- 内核主函数
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 * 		Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/init.h>
#include <photon/irq.h>
#include <photon/sched.h>
#include <photon/smp.h>
#include <photon/cpu.h>
#include <autosar/internal.h>
#include <autosar/common.h>
#include <photon/arch_integrity.h>
#include <asm/can.h>
#include <photon/init_integrity.h>

/**
 * 当前系统启动状态
 * 在不同的启动步骤，系统分配内存的行为有所不同
 */
enum boot_states boot_state;

/**
 * 标记主核已经启动
 */
static void smp_mark_master(void)
{
	int32_t cpu = smp_processor_id();

	MODULE_INIT(smp_mark_master);
	/* 主核总是可用的 */
	mark_cpu_online(cpu, true);
	mark_cpu_present(cpu, true);
	mark_cpu_possible(cpu, true);
}

/**
 * 主核初始化
 */
asmlinkage void start_master(void)
{
	boot_state = BOOTING;

	disable_irq();
	/* 为主核设置其活动掩码 */
	smp_mark_master();
	start_arch();
	/* 体系结构特定的初始化过程 */
	pr_debug("开始初始化硬件架构和内核系统\n");

	init_slave_early();
	init_sched_early();
	init_IRQ();
	init_time();
	arch_init_can();

	init_sched();
	boot_state = KERN_PREPARE_RUN;


	init_timer_arch();
	boot_state = KERN_RUNNING;

	/**
	 * 进入main之前需要执行前期初始化
	 */
	autosar_preinit();
	enable_irq();
	check_timer_validity();
	if (all_module_inited() == 0) {
		panic("初始化不完整，启动失败!!!!!!\n");
	}
	pr_debug("初始化完成，准备进入main函数\n");

	main();
	/* 不可能运行到这里来 */
	BUG();
}
