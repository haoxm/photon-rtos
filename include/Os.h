/*
 * PhotonRTOS础光实时操作系统 -- OS系统总文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_H
#define OS_H

#include <autosar/version.h>

/**
 * 导入功能头文件
 */
#include <autosar/common.h>
#include <autosar/multicore.h>
#include <autosar/schedule_table.h>
#include <autosar/counter.h>
#include <autosar/os_application.h>
#include <autosar/object.h>
#include <autosar/memory.h>
#include <autosar/isr.h>
#include <autosar/peripheral.h>
#include <autosar/spinlock.h>
#include <autosar/task.h>
#include <autosar/alarm.h>
#include <autosar/event.h>
#include <autosar/resource.h>
#include <autosar/hooks.h>
#include <autosar/system.h>

#endif /* OS_H */
