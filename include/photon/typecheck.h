/*
 * PhotonRTOS础光实时操作系统
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef _OS_TYPECHECK_H
#define _OS_TYPECHECK_H

#define typecheck(type, x) \
({	type _tmp_dummy; \
	typeof(x) _tmp_dummy2; \
	(void)(&_tmp_dummy == &_tmp_dummy2); \
	1; \
})

#define typecheck_fn(type, function) \
({	typeof(type) __tmp_dummy = function; \
	(void)__tmp_dummy; \
})

#endif /* _OS_TYPECHECK_H */
