/*
 * PhotonRTOS础光实时操作系统 -- 小端处理
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef PHOTON_BYTEORDER_LITTLE_ENDIAN_H
#define PHOTON_BYTEORDER_LITTLE_ENDIAN_H

#ifndef __LITTLE_ENDIAN
#define __LITTLE_ENDIAN 1234
#endif

#define __cpu_to_le32(x) ((__force __le32)(uint32_t)(x))
#define __le32_to_cpu(x) ((__force uint32_t)(__le32)(x))

#include <photon/byteorder/generic.h>
#endif /* PHOTON_BYTEORDER_LITTLE_ENDIAN_H */
