/*
 * PhotonRTOS础光实时操作系统 -- 初始化完整性检查头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef _OS_INIT_INTEGRITY_H
#define _OS_INIT_INTEGRITY_H

#include <photon/types.h>

enum
{
	MODULE_smp_mark_master = 0,
	MODULE_start_arch,
	MODULE_init_slave_early,
	MODULE_init_sched_early,
	MODULE_init_IRQ,
	MODULE_init_time,
	MODULE_init_sched,
	MODULE_init_timer_arch,
	MODULE_autosar_preinit,
	MODULE_NUM,
};

#define MODULE_INIT(init)		\
	init_module(MODULE_##init);

void init_module(uint8_t module);

uint8_t all_module_inited(void);

#endif /* _OS_ARCG_INTEGRITY_H */