/*
 * PhotonRTOS础光实时操作系统 -- BUG处理
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef PHOTON_BUG_H
#define PHOTON_BUG_H

#include <asm/bug.h>
#include <photon/compiler.h>

#define BUILD_BUG_ON_ZERO(e) (sizeof(struct { int32_t: -!!(e); }))
#define BUILD_BUG_ON_NULL(e) ((void *)sizeof(struct { int32_t: -!!(e); }))

#define BUILD_BUG_ON_MSG(cond, msg) compiletime_assert(!(cond), msg)

#ifndef __OPTIMIZE__
#define BUILD_BUG_ON(condition) ((void)sizeof(char[1 - 2*!!(condition)]))
#else
#define BUILD_BUG_ON(condition)						\
	BUILD_BUG_ON_MSG(condition, "BUILD_BUG_ON failed: " #condition)
#endif

#define BUILD_BUG() BUILD_BUG_ON_MSG(1, "BUILD_BUG failed")

#endif	/* PHOTON_BUG_H */
