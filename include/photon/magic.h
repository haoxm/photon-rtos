/*
 * PhotonRTOS础光实时操作系统 -- 魔法数常量
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef _OS_MAGIC_H
#define _OS_MAGIC_H

#define MAGIC_BASE (0x761203)

#define TASK_MAGIC	(MAGIC_BASE + 0x2)

#define LIST_UNLINK1  ((void *) MAGIC_BASE + 0x3)
#define LIST_UNLINK2  ((void *) MAGIC_BASE + 0x4)

#endif /* _OS_MAGIC_H */
