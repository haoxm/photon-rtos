/*
 * 础光实时操作系统PhotonRTOS -- 环形队列
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: RUi yang<yangrui@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __RING_Queue_H
#define __RING_Queue_H
#include <photon/types.h>
#include <photon/smp_lock.h>

struct ring
{
    bool isFull;
    bool lostData;
    uint32_t head;
    uint32_t tail;
    uint32_t length;
    uint8_t * data;
    struct smp_lock lock;
};

uint32_t ring_get_size(struct ring * ring_info);

uint32_t ring_get_used_size(struct ring * ring_info);
uint32_t ring_get_free_size(struct ring * ring_info);

uint8_t ring_push_element(struct ring * ring_info, uint8_t * data, uint32_t size);
uint8_t ring_pop_element(struct ring * ring_info,  uint8_t * data, uint32_t size);

bool ring_get_lost_data_flag(struct ring * ring_info);
void ring_set_lost_data_flag(struct ring * ring_info, bool lost_data_flag);
void ring_empty(struct ring * ring_info);
void dump_ring_queue(struct ring * ring_info);

#endif