/*
 * PhotonRTOS础光实时操作系统 -- 线程描述符
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef _OS_THREAD_INFO_H
#define _OS_THREAD_INFO_H

#include <photon/bitops.h>
#include <photon/bug_build.h>

#include <asm/process.h>
#include <asm/processor.h>
#include <autosar/object.h>
#ifdef __KERNEL__

enum {
	/**
	 * 有更高优先级的任务被唤醒
	 * 线程将在合适的时候被抢占
	 */
	PROCFLAG_NEED_RESCHED,
	/**
	 * 此任务是否被其它 autosar 任务异步激活
	 */
	PROCFLAG_ACTIVATE_ASYNC_FLAG,
	/**
	 * 此任务是否被其他任务异步设置事件
	 */
	PROCFLAG_SET_EVENT_ASYNC_FLAG,
	/**
	 * 标准的标志数量
	 * 体系结构定义的标志应当从此开始
	 */
	PROCFLAG_COUNT,
};

struct process_desc {
	/**
	 * 必须放在结构体的首部，代表process_desc的父类
	 */
	struct autosar_object object;
	/**
	 * 所在CPU
	*/
	uint8_t			cpu:3;
	/**
	 * 抢占计数
	 */
	uint16_t		preempt_count;
	/**
	 * 当前正在处理的中断号
	 */
	uint16_t current_irq;
	/**
	 * 线程描述符结构
	 */
	struct task_desc		*task;
	/**
	 * 线程标志 ，如PROCFLAG_NEED_RESCHED
	 */
	uintptr_t		flags;
	/**
	 * 体系结构特定的描述符
	 */
	struct arch_process_desc	arch_desc;
	/**
	 * 魔法值
	 */
	int32_t			magic;
};

/**
 * 进程描述符及其堆栈
 */
union process_union {
	struct process_desc process_desc;
	uintptr_t stack[PROCESS_STACK_SIZE/sizeof(intptr_t)];
};

static inline void
__set_process_flag(struct process_desc *proc, int32_t flag)
{
	atomic_set_bit(flag, &proc->flags);
}

static inline void
__clear_process_flag(struct process_desc *proc, int32_t flag)
{
	atomic_clear_bit(flag, &proc->flags);
}


static inline int32_t
__test_process_flag(struct process_desc *proc, int32_t flag)
{
	return test_bit(flag, &proc->flags);
}

#define set_process_flag(flag) \
	__set_process_flag(current_proc_info(), flag)
#define clear_process_flag(flag) \
	__clear_process_flag(current_proc_info(), flag)

#define test_process_flag(flag) \
	__test_process_flag(current_proc_info(), flag)

#define test_task_process_flag(proc, flag) \
	__test_process_flag(proc, flag)

#define get_current() (current_proc_info()->task)
#define current get_current()

#endif	/* __KERNEL__ */

#endif /* _OS_THREAD_INFO_H */
