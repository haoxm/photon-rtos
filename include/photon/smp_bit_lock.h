/*
 * PhotonRTOS础光实时操作系统 -- 位锁
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef PHOTON_SMP_BIT_LOCK_H
#define PHOTON_SMP_BIT_LOCK_H

#include <photon/bitops.h>

extern void smp_bit_lock(int32_t bitnum, uintptr_t *addr);

extern int32_t smp_bit_trylock(int32_t bitnum, uintptr_t *addr);

extern void smp_bit_unlock(int32_t bitnum, uintptr_t *addr);

static inline int32_t smp_bit_is_locked(int32_t bitnum, uintptr_t *addr)
{
	return test_bit(bitnum, addr);
}

#endif /* PHOTON_SMP_BIT_LOCK_H */
