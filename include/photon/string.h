/*
 * PhotonRTOS础光实时操作系统 -- 字符处理
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __OS_STRING_H
#define __OS_STRING_H

#include <photon/types.h>	/* for size_t */
#include <stdarg.h>

extern char *strndup_user(const char  *start, intptr_t count);
extern void *memdup_user(const void  *start, size_t count);

#ifndef __HAVE_ARCH_STRCPY
extern char *strcpy(char *dest, char *src);
#endif

#ifndef __HAVE_ARCH_STRCMP
extern int32_t strcmp(const char *cs, const char *ct);
#endif
#ifndef __HAVE_ARCH_STRNCMP
extern int32_t strncmp(const char *cs, const char *ct, size_t count);
#endif
#ifndef __HAVE_ARCH_STRCASECMP
extern int32_t strcasecmp(const char *s1, const char *s2);
#endif

#ifndef __HAVE_ARCH_STRCHRNUL
extern char *strchrnul(const char *s, int32_t c);
#endif

extern char *skip_spaces(const char *str);
extern char *strim(char *s);

#ifndef __HAVE_ARCH_STRSTR
extern char *strstr(const char *s1, const char *s2);
#endif
#ifndef __HAVE_ARCH_STRNSTR
extern char *strnstr(const char *s1, const char *s2, size_t count);
#endif
#ifndef __HAVE_ARCH_STRLEN
extern size_t strlen(const char *s);
#endif
#ifndef __HAVE_ARCH_STRNLEN
extern size_t strnlen(const char *s, size_t count);
#endif
#ifndef __HAVE_ARCH_STRPBRK
extern char *strpbrk(const char *cs, const char *ct);
#endif
#ifndef __HAVE_ARCH_STRSEP
extern char *strsep(char **s, const char *ct);
#endif
#ifndef __HAVE_ARCH_STRSPN
extern size_t strspn(const char *s, const char *accept);
#endif
#ifndef __HAVE_ARCH_STRCSPN
extern size_t strcspn(const char *s, const char *reject);
#endif

#ifndef __HAVE_ARCH_MEMSET
extern void *memset(void *s, int32_t c, size_t count);
#endif
#ifndef __HAVE_ARCH_MEMCPY
extern void *memcpy(void *dest, const void *src, size_t count);
#endif
#ifndef __HAVE_ARCH_MEMMOVE
extern void *memmove(void *dest, const void *src, size_t count);
#endif
#ifndef __HAVE_ARCH_MEMSCAN
extern void *memscan(void *s, int32_t c, size_t count);
#endif
#ifndef __HAVE_ARCH_MEMCMP
extern int32_t memcmp(const void *s1, const void *s2, size_t count);
#endif
#ifndef __HAVE_ARCH_MEMCHR
extern void *memchr(const void *start, int32_t c, size_t bytes);
#endif

#ifdef CONFIG_BINARY_PRINTF
int32_t vbin_printf(uint32_t *bin_buf, size_t size, const char *fmt, va_list args);
int32_t bstr_printf(char *buf, size_t size, const char *fmt, const uint32_t *bin_buf);
int32_t bprintf(uint32_t *bin_buf, size_t size, const char *fmt, ...);
#endif

/* lib/printf utilities */

extern int32_t sprintf(char *buf, const char *fmt, ...);
extern  int32_t vsprintf(char *buf, const char *, va_list);
extern int32_t snprintf(char *buf, size_t size, const char *fmt, ...);
extern int32_t vsnprintf(char *buf, size_t size, const char *fmt, va_list args);
extern int32_t scnprintf(char *buf, size_t size, const char *fmt, ...);
extern int32_t vscnprintf(char *buf, size_t size, const char *fmt, va_list args);


/* Obsolete, do not use.  Use kstrto<foo> instead */

extern uintptr_t simple_strtoul(const char *cp, char **endp, uint32_t base);
extern intptr_t simple_strtol(const char *cp, char **endp, uint32_t base);
extern uint64_t simple_strtoull(const char *cp, char **endp, uint32_t base);
extern int64_t simple_strtoll(const char *cp, char **endp, uint32_t base);

extern int32_t num_to_str(char *buf, int32_t size, uint64_t num);

#endif /* __OS_STRING_H */
