#ifndef OS_AUTOSAR_IOC_TABLE_CONFIG_H
#define OS_AUTOSAR_IOC_TABLE_CONFIG_H

#define OS_IOC_SET_BUFFER_SIZE(BufferSize)          .RingInfo.length = BufferSize
#define OS_IOC_SET_BUFFER_POINT(BufferPoint)        .RingInfo.data = BufferPoint
#define OS_IOC_SET_CALLBACK_POINT(CallBackPoint)    .FunctionCb = CallBackPoint

typedef VAR(struct InnerQueueGroup,TYPEDEF)     OsIocBufferConfigType;

void config_to_ioc(void);

#endif