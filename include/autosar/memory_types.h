/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR内存类型
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Jiayuan Liang <liangjiayuan@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_MEMORY_TYPES_H
#define OS_MEMORY_TYPES_H

#include <autosar/common_types.h>

typedef P2VAR(void, TYPEDEF, OS_APPL_DATA) MemoryStartAddressType;
typedef VAR(uint32, TYPEDEF) MemorySizeType;

#endif /* OS_MEMORY_TYPES_H */
