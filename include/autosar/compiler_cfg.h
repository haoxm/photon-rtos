/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR编译器抽象配置
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_AUTOSAR_COMPILER_CFG_H
#define OS_AUTOSAR_COMPILER_CFG_H

#if (defined(__GNUC__))
#include <autosar/compiler_cfg_gcc.h>
#else
#error Unsupported Compiler!
#endif /* __GNUC__ */

#endif /* OS_AUTOSAR_COMPILER_CFG_H */
