/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR应用接口
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Jiayuan Liang <liangjiayuan@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_APPLICATION_H
#define OS_APPLICATION_H

#include <autosar/os_application_types.h>

#if (defined(AUTOSAR_OS_APPLICATION) || defined(AUTOSAR_MULTICORE))
/**
 * GetApplicationID()应返回配置执行任务/ISR/hook 的应用程序标识符。
 * 语法：
 *      ApplicationType GetApplicationID ( void )
 * 服务 ID [hex] :
 *      0x00
 * 同步/异步 :
 *      同步（Synchronous）
 * 可重入性:
 *      可重入
 * 参数（输入）：
 *      无
 * 参数（输出）：
 *      无
 * 返回值：
 *      <运行 OS-Application 的标识符> 或 INVALID_OSAPPLICATION
 * 描述：
 *      该服务确定调用者最初所属（被配置为）的操作系统应用程序（
 *      必须为每个应用程序分配一个唯一标识符）。
 * 引用：
 *      AUTOSAR OS 8.4.1 GetApplicationID [SWS_Os_00016]
 */
FUNC(ApplicationType, OS_CODE) GetApplicationID(void);
#endif /* (defined(AUTOSAR_OS_APPLICATION) || defined(AUTOSAR_MULTICORE)) */

#if defined(AUTOSAR_OS_APPLICATION)
/**
 * GetCurrentApplicationID()应返回执行当前任务/ISR/hook 的应用程序标识符。
 * 语法：
 *      ApplicationType GetCurrentApplicationID ( void )
 * 服务 ID [hex] :
 *      0x27
 * 同步/异步 :
 *      同步（Synchronous）
 * 可重入性:
 *      可重入
 * 参数（输入）：
 *      无
 * 参数（输出）：
 *      无
 * 返回值：
 *      ApplicationType  <运行 OS-Application 的标识符> 或 INVALID_OSAPPLICATION
 * 描述：
 *      该服务确定服务调用者当前正在执行的操作系统应用程序。请注意，
 *      如果调用者不在 CallTrustedFunction() 调用中，
 *      则该值等于 GetApplicationID() 的结果。
 * 引用：
 *      AUTOSAR OS 8.4.2 GetCurrentApplicationID [SWS_Os_00797]
 */
FUNC(ApplicationType, OS_CODE) GetCurrentApplicationID(void);
#endif /* defined(AUTOSAR_OS_APPLICATION) */

#if defined(AUTOSAR_TRUSTED_FUNCTION)
/**
 * 如果 <FunctionIndex> 是一个定义的函数索引，
 * CallTrustedFunction()将调用函数 <FunctionIndex>
 * 从实现特定可信函数列表中使用提供可信函数的
 *  OSApplication 的保护设置，并在完成后返回E_OK 。
 * 语法：
 *      StatusType CallTrustedFunction (
 *              TrustedFunctionIndexType FunctionIndex,
 *              TrustedFunctionParameterRefType FunctionParams )
 * 服务 ID [hex] :
 *      0x02
 * 同步/异步 :
 *      取决于被调用的函数。如果调用的函数是同步的，那么服务是同步的
 * 。可能会导致重新安排。
 * 可重入性:
 *      可重入
 * 参数（输入）：
 *      Function Index 要调用的函数的索引。
 *      Function Params 指向要调用的函数的参数的指针 - 由函数索引指定。
 *              如果没有提供参数，则必须传递 NULL 指针。
 * 参数（输出）：
 *      无
 * 返回值：
 *      StatusType
 *      E_OK：无错误
 *      E_OS_SERVICEID：没有为此索引定义函数
 * 描述：
 *      一个（可信或不可信的）OS-Application 使用这个服务来调用一个可信函
 * 数
 * 引用：
 *      AUTOSAR OS 8.4.4 CallTrustedFunction [SWS_Os_00097]
 */
FUNC(StatusType, OS_CODE) CallTrustedFunction(
	VAR(TrustedFunctionIndexType, AUTOMATIC) FunctionIndex,
	VAR(TrustedFunctionParameterRefType, AUTOMATIC) FunctionParams);
#endif /* defined(AUTOSAR_TRUSTED_FUNCTION) */

#if defined(AUTOSAR_OS_APPLICATION)
/**
 * 服务名字
 *      TerminateApplication
 * 语法
 *		StatusType TerminateApplication (
 *           ApplicationType Application,
 *           RestartType RestartOption )
 * 服务 ID [hex]
 *		0x12
 * 同步/异步
 *		同步（Synchronous）
 * 可重入性
 *		可重入
 * 参数 (in)
 *	  Application 要终止的 OS 应用程序的标识符。如果调用者属于
 *  <Application>，则调用会导致自终止。
 *      Restart Option  RESTART 用于重新启动 OS-Application 或 NO_RESTART 如果
 *  OS-Application 不应重新启动。
 * 参数 (inout)
 *		None
 * 参数 (out)
 *		None
 * 返回值
 *      StatusType
 *		 E_OK：没有错误
 *   E_OS_ID：<Application> 无效（仅处于 EXTENDED 状态）
 *   E_OS_VALUE：<RestartOption> 既不是 RESTART 也不是 NO_
 *   RESTART（仅在 EXTENDED 状态下）
 *   E_OS_ACCESS：调用者无权终止<Application>（仅在 EXTENDED 状态下）
 *   E_OS_STATE：<Application> 的状态不允许终止<Application>
 * 描述
 *	 该服务终止调用任务/类别2 ISR/应用程序特定错误钩子所属的操作系
 * 统应用程序
 * 引用
 *		Os.h
 *      AUTOSAR OS 8.4.20 TerminateApplication [SWS_Os_00258]
 */
FUNC(StatusType, OS_CODE) TerminateApplication(
	VAR(ApplicationType, AUTOMATIC) Application,
	VAR(RestartType, AUTOMATIC) RestartOption);

/**
 * 服务名字
 *      AllowAccess
 * 语法
 *		StatusType AllowAccess (void)
 * 服务 ID [hex]
 *		0x13
 * 同步/异步
 *		同步（Synchronous）
 * 可重入性
 *		可重入
 * 参数 (in)
 *  None
 * 参数 (inout)
 *		None
 * 参数 (out)
 *		None
 * 返回值
 *      StatusType
 *		 E_OK：没有错误
 *   E_OS_STATE：调用者的OS-Application处于错误状态
 * 描述
 *	 该服务将 OS-Application 的自身状态从 APPLICATION_RESTARTING 设置为
 *  APPLICATION_ACCESSIBLE。
 * 引用
 *		Os.h
 *      AUTOSAR OS 8.4.21 AllowAccess [SWS_Os_00501]
 */
FUNC(StatusType, OS_CODE) AllowAccess(void);

/**
 * 服务名字
 *      GetApplicationState
 * 语法
 *		StatusType GetApplicationState (
 *           ApplicationType Application,
 *           ApplicationStateRefType Value )
 * 服务 ID [hex]
 *		0x14
 * 同步/异步
 *		同步（Synchronous）
 * 可重入性
 *		可重入
 * 参数 (in)
 *      Application 请求状态的 OS-Application
 * 参数 (inout)
 *		None
 * 参数 (out)
 *	Value  应用程序当前的状态
 * 返回值
 *      StatusType
 *		 E_OK：没有错误
 *   E_OS_ID：<Application> 无效（仅在 EXTENDED 状态下）
 * 描述
 *		此服务返回 OS 应用程序的当前状态。
 * 引用
 *		Os.h
 *      AUTOSAR OS 8.4.22 GetApplicationState [SWS_Os_00499]
 */
FUNC(StatusType, OS_CODE) GetApplicationState(
	VAR(ApplicationType, AUTOMATIC) Application,
	VAR(ApplicationStateRefType, AUTOMATIC) Value);

#endif /* defined(AUTOSAR_OS_APPLICATION) */

#endif /* OS_APPLICATION_H */
