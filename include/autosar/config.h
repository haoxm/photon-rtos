/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR系统配置
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_AUTOSAR_UAPI_CONFIG_H
#define OS_AUTOSAR_UAPI_CONFIG_H

#if defined(CONFIG_AUTOSAR_SCALABILITY_CLASS_1)
#if !defined(CONFIG_AUTOSAR_ALARM_CALLBACK)
/* #warning Alarm Callback is not used in SCALABILITY_CALSS_1 */
#endif

#if defined(CONFIG_AUTOSAR_TIMING_PROTECTION)
#error Timing Protection is not allowed in Scalability Class 1
#endif

#if defined(CONFIG_AUTOSAR_GLOBAL_TIME)
#error Global Time is not allowed in Scalability Class 1
#endif

#if defined(CONFIG_AUTOSAR_SYNCHRONIZATION_SUPPORT)
#error Synchronizzation Support is not allowed in Scalability Class 1
#endif

#if defined(CONFIG_EXTENDED_STATUS)
/* #warning Extended Status is used in Scalability Class 1 */
#endif

#if defined(CONFIG_AUTOSAR_OS_APPLICATION)
#error OS Application is not allowed in Scalability Class 1
#endif

#if defined(CONFIG_AUTOSAR_MEMORY_PROTECTION)
#error Memory Protection is not allowed in Scalability Class 1
#endif

#if defined(CONFIG_AUTOSAR_SERVICE_PROTECTION)
#error Service Protection is not allowed in Scalability Class 1
#endif

#if defined(CONFIG_AUTOSAR_TRUSTED_FUNCTION)
#error Trusted Function is not allowed in Scalability Class 1
#endif



#elif defined(CONFIG_AUTOSAR_SCALABILITY_CLASS_2)
#if defined(CONFIG_AUTOSAR_ALARM_CALLBACK)
#error Alarm Callback is not allowed in Scalability Class 2
#endif

#if !defined(CONFIG_AUTOSAR_TIMING_PROTECTION)
/* #warning Timing Protection is not used in SCALABILITY_CALSS_2 */
#endif

#if !defined(CONFIG_AUTOSAR_GLOBAL_TIME)
/* #warning Global Time is not used in Scalability Class 2 */
#endif

#if !defined(CONFIG_AUTOSAR_SYNCHRONIZATION_SUPPORT)
/* #warning Synchronizzation Support is not used in Scalability Class 2 */
#endif

#if defined(CONFIG_EXTENDED_STATUS)
/* #warning Extended Status is used in SCALABILITY_CALSS_2 */
#endif

#if defined(CONFIG_AUTOSAR_OS_APPLICATION)
#error OS Application is not allowed in SCALABILITY_CALSS_2
#endif

#if defined(CONFIG_AUTOSAR_MEMORY_PROTECTION)
#error Memory Protection is not allowed in SCALABILITY_CALSS_2
#endif

#if defined(CONFIG_AUTOSAR_SERVICE_PROTECTION)
#error Service Protection is not allowed in SCALABILITY_CALSS_2
#endif

#if defined(CONFIG_AUTOSAR_TRUSTED_FUNCTION)
#error Trusted Function is not allowed in SCALABILITY_CALSS_2
#endif



#elif defined(CONFIG_AUTOSAR_SCALABILITY_CLASS_3)
#if defined(CONFIG_AUTOSAR_ALARM_CALLBACK)
#error Alarm Callback is not allowed in Scalability Class 3
#endif

#if defined(CONFIG_AUTOSAR_TIMING_PROTECTION)
#error Timing Protection is not allowed in Scalability Class 3
#endif

#if defined(CONFIG_AUTOSAR_GLOBAL_TIME)
#error Global Time is not allowed in Scalability Class 3
#endif

#if defined(CONFIG_AUTOSAR_SYNCHRONIZATION_SUPPORT)
#error Synchronizzation Support is not allowed in Scalability Class 3
#endif

#if !defined(CONFIG_EXTENDED_STATUS)
#define CONFIG_EXTENDED_STATUS	1
/* #warning Extended Status must be used in Scalability Class 3, auto open Extended Status now!!! */
#endif

#if !defined(CONFIG_AUTOSAR_OS_APPLICATION)
/* #warning OS Application is not used in Scalability Class 3 */
#endif

#if !defined(CONFIG_AUTOSAR_MEMORY_PROTECTION)
/* #warning Memory Protection is not used in Scalability Class 3 */
#endif

#if !defined(CONFIG_AUTOSAR_SERVICE_PROTECTION)
/* #warning Service Protection is not used in Scalability Class 3 */
#endif

#if !defined(CONFIG_AUTOSAR_TRUSTED_FUNCTION)
/* #warning Trusted Function is not used in Scalability Class 3 */
#endif



#elif defined(CONFIG_AUTOSAR_SCALABILITY_CLASS_4)
#if defined(CONFIG_AUTOSAR_ALARM_CALLBACK)
#error Alarm Callback is not allowed in Scalability Class 4
#endif

#if !defined(CONFIG_AUTOSAR_TIMING_PROTECTION)
/* #warning Timing Protection is not used in Scalability Class 4 */
#endif

#if !defined(CONFIG_AUTOSAR_GLOBAL_TIME)
/* #warning Global Time is not used in Scalability Class 2 */
#endif

#if !defined(CONFIG_AUTOSAR_SYNCHRONIZATION_SUPPORT)
/* #warning Synchronizzation Support is not used in Scalability Class 2 */
#endif

#if !defined(CONFIG_EXTENDED_STATUS)
#define CONFIG_EXTENDED_STATUS	1
/* #warning Extended Status must be used in Scalability Class 4, auto open Extended Status now!!! */
#endif

#if !defined(CONFIG_AUTOSAR_OS_APPLICATION)
/* #warning OS Application is not used in Scalability Class 4 */
#endif

#if !defined(CONFIG_AUTOSAR_MEMORY_PROTECTION)
/* #warning Memory Protection is not used in Scalability Class 4 */
#endif

#if !defined(CONFIG_AUTOSAR_SERVICE_PROTECTION)
/* #warning Service Protection is not used in Scalability Class 4 */
#endif

#if !defined(CONFIG_AUTOSAR_TRUSTED_FUNCTION)
/* #warning Trusted Function is not used in Scalability Class 4 */
#endif

#endif


#if defined(CONFIG_AUTOSAR_ALARM_CALLBACK)
#define AUTOSAR_ALARM_CALLBACK
#endif

#if defined(CONFIG_AUTOSAR_TIMING_PROTECTION)
#define AUTOSAR_TIMING_PROTECTION
#endif

/**
 * TODO: 该模块功能暂未实现
*/
#if defined(CONFIG_AUTOSAR_GLOBAL_TIME)
#define AUTOSAR_GLOBAL_TIME
#endif

#if defined(CONFIG_AUTOSAR_SYNCHRONIZATION_SUPPORT)
#define AUTOSAR_SYNCHRONIZATION_SUPPORT
#endif

#if defined(CONFIG_EXTENDED_STATUS)
#define EXTENDED_STATUS
#endif

#if defined(CONFIG_AUTOSAR_OS_APPLICATION)
#define AUTOSAR_OS_APPLICATION
#endif

#if defined(CONFIG_AUTOSAR_MEMORY_PROTECTION)
#define AUTOSAR_MEMORY_PROTECTION
#endif

/**
 * TODO: 该模块功能暂未实现
*/
#if defined(CONFIG_AUTOSAR_SERVICE_PROTECTION)
#define AUTOSAR_SERVICE_PROTECTION
#endif

#if defined(CONFIG_AUTOSAR_TRUSTED_FUNCTION)
#define AUTOSAR_TRUSTED_FUNCTION
#endif

#if defined(CONFIG_AUTOSAR_SHOW_TASK_INFO)
#define AUTOSAR_SHOW_TASK_INFO
#endif

#if (defined(CONFIG_MAX_CPUS) && defined(CONFIG_NON_AUTOSAR_CORES)	\
		&& ((CONFIG_MAX_CPUS - CONFIG_NON_AUTOSAR_CORES) > 1))
#define AUTOSAR_MULTICORE
#endif

#endif

