/*
 * PhotonRTOS础光实时操作系统 -- OS系统版本
 *
 * Copyright (C) 2022, 2023, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_VERSION_H
#define OS_VERSION_H

#define OS_VENDOR_ID        0u      /* Unknown AUTOSAR Vendor ID  */
#define OS_MODULE_ID        0x01    /* OS Module ID                 */

#define OS_AR_MAJOR_VERSION 20  /* Major version number of AUTOSAR specification       */
#define OS_AR_MINOR_VERSION 11  /* Minor version number of AUTOSAR specification       */
#define OS_AR_PATCH_VERSION 0   /* Patch level version number of AUTOSAR specification */

#define OS_SW_MAJOR_VERSION 0   /* Major version number of the implementation   */
#define OS_SW_MINOR_VERSION 95  /* Minor version number of the implementation   */
#define OS_SW_PATCH_VERSION 0   /* Patch level version number of the implementation */

#endif /* OS_VERSION_H */
