/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR计数器配置
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_COUNTER_CONFIG_H
#define OS_COUNTER_CONFIG_H

#define DeclareCounter(CounterID, Max_value, Ticks_per_base, Min_cycle, Type)	\
	autosar_counters[CounterID].tick = 0;					\
	autosar_counters[CounterID].value = 0;					\
	list_init(&(autosar_counters[CounterID].waiters_list));			\
	smp_lock_init(&(autosar_counters[CounterID].lock));			\
	autosar_counters[CounterID].next_waiter = NULL;				\
	autosar_counters[CounterID].core = GetCoreID();				\
	autosar_counters[CounterID].max_value = Max_value;			\
	autosar_counters[CounterID].ticks_per_base = Ticks_per_base;		\
	autosar_counters[CounterID].min_cycle = Min_cycle;			\
	autosar_counters[CounterID].type = Type;				\


#define DeclareWaiter(WaiterID, Time, Cycle, CounterID, Callback, Param)	\
	autosar_waiters[WaiterID].time = Time;					\
	autosar_waiters[WaiterID].cycle = Cycle;				\
	add_waiter_to_counter(&(autosar_waiters[WaiterID]), CounterID);		\
	autosar_waiters[WaiterID].callback = Callback;				\
	autosar_waiters[WaiterID].param = Param;				\

#endif