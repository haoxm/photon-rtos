/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR事件类型
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_EVENT_TYPES_H
#define OS_EVENT_TYPES_H

#include <autosar/common_types.h>

typedef VAR(uint32, TYPEDEF) EventMaskType;
typedef P2VAR(EventMaskType, TYPEDEF,OS_APPL_DATA) EventMaskRefType;

#endif /* OS_EVENT_TYPES_H */
