/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR多核接口
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_MULTICORE_H
#define OS_MULTICORE_H

#include <autosar/multicore_types.h>

/**
 * 核心在空闲时间不执行任何特定操作
 */
#define IDLE_NO_HALT 0

/**
 * GetCoreID 函数应返回调用该函数的核心的唯一逻辑 CoreID。
 * 物理核心到逻辑 CoreID 的映射是特殊实现的。
 * 语法：
 *      CoreIdType GetCoreID ( void )
 * 参数（输入）：
 *      无
 * 参数（输出）：
 *      无
 * 返回值：
 *      返回当前的核心ID。
 * 引用：
 *      AUTOSAR OS 8.4.24 GetCoreID [SWS_Os_00674]
 */
FUNC(CoreIdType, OS_CODE) GetCoreID(void);

/**
 * 服务名字
 *      GetNumberOfActivatedCores
 * 语法
 *      uint32 GetNumberOfActivatedCores ( void )
 * 服务 ID [hex]
 *      0x15
 * 同步/异步
 *      同步（Synchronous）
 * 可重入性
 *      可重入
 * 参数 (in)
 *      None
 * 参数 (inout)
 *      None
 * 参数 (out)
 *      None
 * 返回值
 *      uint32		StartCore 函数激活的核心数（见下文）
 * 描述
 *      该函数返回由 StartCore 函数激活的核心数。这个函数可能是一个宏。
 * 引用
 *      Os.h
 *      AUTOSAR OS 8.4.23 GetNumberOfActivatedCores [SWS_Os_00672]
 */
FUNC(uint32, OS_CODE) GetNumberOfActivatedCores(void);

/**
 * 服务名字
 *      StartCore
 * 语法
 *		void StartCore (
 *          CoreIdType CoreID,
 *          StatusType* Status
 *      )
 * 服务 ID [hex]
 *		0x17
 * 同步/异步
 *		同步（Synchronous）
 * 可重入性
 *      不可重入
 * 参数 (in)
 *		CoreID		核心标识符
 * 参数 (inout)
 *		    None
 * 参数 (out)
 *		Status		扩展状态函数返回值：
 *          E_OK: No Error
 *          E_OS_ID: Core ID 无效。
 *          E_OS_ACCESS：启动操作系统后调用该函数。
 *          E_OS_STATE：核心已经激活。
 *      标准状态下函数返回值
 *          E_OK: No Error
 * 返回值
 *      None
 * 描述
 *	  不支持在 StartOS() 之后调用该函数。该函数启动由参数CoreID 指定的
 * 核心。
 *      OUT 参数允许调用者检查操作是否成功。
 *      如果通过此函数启动核心，则应在核心上调用 StartOS。
 * 引用
 *		Os.h
 *      AUTOSAR OS 8.4.25 StartCore [SWS_Os_00676]
 */
FUNC(void, OS_CODE) StartCore(
	VAR(CoreIdType, AUTOMATIC) CoreID,
	P2VAR(StatusType, AUTOMATIC, OS_APPL_DATA) Status);

/**
 * Service Name
 *		StartNonAutosarCore
 * 语法
 *		void StartNonAutosarCore (
 *              CoreIdType CoreID,
 *              StatusType* Status
 *      )
 * 服务 ID [hex]
 *		0x18
 * 同步/异步
 *		同步（Synchronous）
 * 可重入性
 *		不可重入
 * 参数 (in)
 *		CoreID		核心标识符
 * 参数 (inout)
 *		None
 * 参数 (out)
 *		Status		标准状态下函数返回值：
 *              E_OK: No Error
 *              E_OS_ID: Core ID 无效。
 *              E_OS_STATE：核心已经激活。
 * 扩展状态下函数返回值
 *      E_OK: No Error
 * 返回值
 *		None
 * 描述
 *	 该函数启动由参数CoreID 指定的核心。允许在 StartOS() 之后调用该函
 * 数。
 *      OUT 参数允许调用者检查操作是否成功。不允许在由 StartNonAutosarCore 激
 * 活的核心上调用 StartOS。
 * 否则行为是未指定的。
 * 引用
 *		Os.h
 *      AUTOSAR OS 8.4.26 StartCore [SWS_Os_00682]
 */
FUNC(void, OS_CODE) StartNonAutosarCore(
	VAR(CoreIdType, AUTOMATIC) CoreID,
	P2VAR(StatusType, AUTOMATIC, OS_APPL_DATA) Status);


/**
 * 服务名字
 *      ShutdownAllCores
 * 语法
 *		void ShutdownAllCores (
 *              StatusType Error
 *      )
 * 服务 ID [hex]
 *		0x1c
 * 同步/异步
 *		同步（Synchronous）
 * 可重入性
 *		可重入
 * 参数 (in)
 *		Error		<Error> 必须是 AUTOSAR OS 支持的有效错误代码。
 * 参数 (inout)
 *		None
 * 参数 (out)
 *		None
 * 返回值
 *		None
 * 描述
 *		在此服务之后，所有 AUTOSAR 核心上的操作系统都将关闭。
 *      允许在 TASK 级别和 ISR 级别以及操作系统内部调用。该函数将永远不
 * 会返回。该功能将强制其他核心关闭。
 * 引用
 *		Os.h
 *      AUTOSAR OS 8.4.30 ShutdownAllCores [SWS_Os_00713]
 */
FUNC(void, OS_CODE) ShutdownAllCores(VAR(StatusType, AUTOMATIC) Error);

/**
 * Service Name
 *		ControlIdle
 * 语法
 *		StatusType ControlIdle (
 *              CoreIdType CoreID,
 *              IdleModeType IdleMode
 *      )
 * 服务 ID [hex]
 *		0x1d
 * 同步/异步
 *		同步（Synchronous）
 * 可重入性
 *		不可重入
 * 参数 (in)
 *		CoreID		选择设置空闲模式的核心
 *		IdleMode		在空闲时间应执行的模式
 * 参数 (inout)
 *		None
 * 参数 (out)
 *		None
 * 返回值
 *      StatusType
 *		E_OK：无错误
 *              E_OS_ID（仅 EXTENDED 状态）：无效核心和/或无效空闲模式
 * 描述
 *	 此 API 允许调用者选择在操作系统空闲时间期间执行的空闲模式操
 * 作（例如，如果没有任务/ISR 处于活动状态）。
 *      可用于实现节能。真正的空闲模式依赖于硬件并且没有标准化。每个
 * 核心的默认空闲模式是 IDLE_NO_HALT。
 * 引用
 *		Os.h
 *      AUTOSAR OS 8.4.31 ControlIdle [SWS_Os_00769]
 */
FUNC(StatusType, OS_CODE) ControlIdle(
	VAR(CoreIdType, AUTOMATIC) CoreID,
	VAR(IdleModeType, AUTOMATIC) IdleMode);

#endif /* OS_MULTICORE_H */
