显示任务信息`ShowTaskInfo()`
==========

## 1. 函数声明及位置
声明位置 `../include/autosar/osek.h`
```c
/**
 * 显示任务信息
 * base:    BASE_INFO(基本信息)，EXT_INFO(拓展信息)
 * osek_id: ALL_TASK(所有任务)，0,1,2...
 * 
 * 显示所有任务信息时，返回显示的任务数
 * 显示指定任务信息时，返回osek_id表示显示失败，返回osek+1表示显示成功
 */
uintptr_t ShowTaskInfo(uint32_t base, uintptr_t osek_id);
```
## 2. 使用说明
使用前需开启配置 CONFIG_AUTOSAR_SHOW_TASK_INFO
### 2.1 显示任务基本信息
```c
// 输出所有任务的基本信息
ShowTaskInfo(BASE_INFO, ALL_TASK);

// 输出指定任务的基本信息
ShowTaskInfo(BASE_INFO, task_id);
```

输出内容(以所有任务为例)
```
任务基本信息（系统已运行： 0 秒）
core_id: 0
    app_id: 0
          ID   state curr_run_time(us)  time(ms)  prio   stack used name
           0 running        0/   10000       108  1/ 1  0x2000  19% osek[00]
           1 suspend        0/   10000         1  2/ 2  0x2000   6% osek[01]
    app_id: 2
          ID   state curr_run_time(us)  time(ms)  prio   stack used name
          14 suspend        0/   10000         0  3/ 3  0x2000   0% osek[14]
          15 suspend        0/   10000         0  3/ 3  0x2000   0% osek[15]
    app_id: 5
          ID   state curr_run_time(us)  time(ms)  prio   stack used name
           6 suspend        0/   10000         0  3/ 3  0x2000   0% osek[06]
           7 suspend        0/   10000         0  3/ 3  0x2000   0% osek[07]
           8 suspend        0/   10000         0  3/ 3  0x2000   0% osek[08]
           9 suspend        0/   10000         0  3/ 3  0x2000   0% osek[09]
core_id: 1
    app_id: 1
          ID   state curr_run_time(us)  time(ms)  prio   stack used name
           2 suspend        0/   10000         0  3/ 3  0x2000   0% osek[02]
    app_id: 3
          ID   state curr_run_time(us)  time(ms)  prio   stack used name
          13 suspend        0/   10000         0  3/ 3  0x2000   0% osek[13]
    app_id: 4
          ID   state curr_run_time(us)  time(ms)  prio   stack used name
          10 suspend        0/   10000         0  3/ 3  0x2000   0% osek[10]
          11 suspend        0/   10000         0  3/ 3  0x2000   0% osek[11]
          12 suspend        0/   10000         0  3/ 3  0x2000   0% osek[12]
    app_id: 6
          ID   state curr_run_time(us)  time(ms)  prio   stack used name
           3 suspend        0/   10000         0  3/ 3  0x2000   0% osek[03]
           4 suspend        0/   10000         0  3/ 3  0x2000   0% osek[04]
           5 suspend        0/   10000         0  3/ 3  0x2000   0% osek[05]
```

输出内容说明  
| 属性名             | 属性说明                                    | 其他
| ----------------- | ------------------------------------------ | ------
| core_id           | 任务运行的核ID                               |
| app_id            | 任务所属应用ID                               |
| ID                | 任务ID                                      |
| state             | 任务状态                                     | running waiting suspend ready
| curr_run_time(us) | 本次或上次激活后的运行时间 / 每次激活的运行时间预算 | 单位微秒
| time(ms)          | 任务多次激活的总计运行时间                      | 单位毫秒
| prio              | 任务当前优先级 / 任务配置的优先级               | 天花板协议可能导致两者不同
| stack             | 任务堆栈大小                                 | 数值为16进制
| used              | 已使用堆栈百分比                              | 精确到1%，舍弃小数
| name              | 任务名称                                    |


### 2.2 显示任务拓展信息
```c
// 输出所有任务的拓展信息
ShowTaskInfo(EXT_INFO, ALL_TASK);

// 输出指定任务的拓展信息
ShowTaskInfo(EXT_INFO, task_id);
```

输出内容(以所有任务为例)
```
任务拓展信息（系统已运行： 0 秒）
core_id: 0
    app_id: 0
          ID ext active resource resource_mask event_mask event_wait app_mode auto_start name
           0   Y   1/ 8        0        0x0000     0x0000     0x0000  default          Y osek[00]
           1   Y   0/ 8        0        0x0000     0x0000     0x0000  default          N osek[01]
    app_id: 2
          ID ext active resource resource_mask event_mask event_wait app_mode auto_start name
          14   Y   0/ 8        0        0x0000     0x0000     0x0000  default          N osek[14]
          15   Y   0/ 8        0        0x0000     0x0000     0x0000  default          N osek[15]
    app_id: 5
          ID ext active resource resource_mask event_mask event_wait app_mode auto_start name
           6   Y   0/ 8        0        0x0000     0x0000     0x0000  default          N osek[06]
           7   Y   0/ 8        0        0x0000     0x0000     0x0000  default          N osek[07]
           8   Y   0/ 8        0        0x0000     0x0000     0x0000  default          N osek[08]
           9   Y   0/ 8        0        0x0000     0x0000     0x0000  default          N osek[09]
core_id: 1
    app_id: 1
          ID ext active resource resource_mask event_mask event_wait app_mode auto_start name
           2   Y   0/ 8        0        0x0000     0x0000     0x0000  default          N osek[02]
    app_id: 3
          ID ext active resource resource_mask event_mask event_wait app_mode auto_start name
          13   Y   0/ 8        0        0x0000     0x0000     0x0000  default          N osek[13]
    app_id: 4
          ID ext active resource resource_mask event_mask event_wait app_mode auto_start name
          10   Y   0/ 8        0        0x0000     0x0000     0x0000  default          N osek[10]
          11   Y   0/ 8        0        0x0000     0x0000     0x0000  default          N osek[11]
          12   Y   0/ 8        0        0x0000     0x0000     0x0000  default          N osek[12]
    app_id: 6
          ID ext active resource resource_mask event_mask event_wait app_mode auto_start name
           3   Y   0/ 8        0        0x0000     0x0000     0x0000  default          N osek[03]
           4   Y   0/ 8        0        0x0000     0x0000     0x0000  default          N osek[04]
           5   Y   0/ 8        0        0x0000     0x0000     0x0000  default          N osek[05]
```

输出内容说明
| 属性名         | 属性说明                         | 其他
| ------------- | ------------------------------- | ------
| core_id       | 任务运行的核ID                    |
| app_id        | 任务所属应用ID                    |
| ID            | 任务ID                           |
| ext           | 任务是否为拓展任务                 |
| active        | 任务当前激活次数 / 任务最大可激活次数 | 不是累积激活次数，任务结束激活次数会减一
| resource      | 任务持有的资源数量                 | 
| resource_mask | 任务持有资源的掩码                 | 数值为16进制
| event_mask    | 任务已有事件的掩码                 | 数值为16进制
| event_wait    | 任务等待事件的掩码                 | 数值为16进制
| app_mode      | 任务的应用模式                    |
| auto_start    | 任务是否开机自启                  |
| name          | 任务名称                         |