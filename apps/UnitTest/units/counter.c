/*
 * PhotonRTOS础光实时操作系统 -- counter测试代码
 *
 * Copyright (C) 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include "../utest.h"

TEST(IncrementCounter)
{
	VAR(StatusType, AUTOMATIC) st;

#if defined(EXTENDED_STATUS)
	/**
	 * [SWS_Os_00285] ⌈如果IncrementCounter ()调用中的输入参数 <CounterID>无效或计数器是硬件计数器，则IncrementCounter()应返回E_OS_ID 。 ⌋ ( )
	 */
	st = IncrementCounter(AUTOSAR_NR_COUNTER);
	ASSERT_EQ(st, E_OS_ID);
#endif

	/**
	 * [SWS_Os_00286] ⌈如果IncrementCounter ()的输入参数有效，则IncrementCounter()应将计数器 <CounterID> 加一（如果连接到此计数器的任何闹钟到期，则完成给定的操作，例如任务激活）并应返回E_OK 。 ⌋ ( SRS_Os_11020 )
	 */
	DeclareCounter(2, 1024, 2, 1, SOFTWARE);
	st = IncrementCounter(2);
	ASSERT_EQ(st, E_OK);

	/**
	 * [SWS_Os_00321] ⌈如果在调用IncrementCounter () 的过程中发生错误，例如由于任务激活导致的E_OS_LIMIT ，则IncrementCounter () 应调用错误钩子，但IncrementCounter()服务本身应返回E_OK 。 ⌋ ( )
	 */


	/**
	 * [SWS_Os_00529] ⌈ IncrementCounter ()的注意事项：如果从任务中调用，可能会发生重新调度。 ⌋ ( )
	 */


	/**
	 * [SWS_Os_00530] ⌈ IncrementCounter ()的可用性：可用于所有可扩展性类。 ⌋ ( )
	 */
}

TEST(GetCounterValue)
{
	VAR(TickType, AUTOMATIC) value;
	VAR(StatusType, AUTOMATIC) st;

#if defined(EXTENDED_STATUS)
	/**
	 * [SWS_Os_00376] ⌈如果	GetCounterValue ()调用中的输入参数 <CounterID>无效，则GetCounterValue () 应返回E_OS_ID 。 ⌋ ( )
	 */
	st = GetCounterValue(AUTOSAR_NR_COUNTER, &value);
	ASSERT_EQ(st, E_OS_ID);
#endif

	/**
	 * [SWS_Os_00377] ⌈如果GetCounterValue ()调用中的输入参数 <CounterID>有效， GetCounterValue()将通过 <Value> 返回计数器的当前刻度值并返回E_OK 。 ⌋ ( SRS_Frt_00033 )
	 */
	value = 12345;
	DeclareCounter(2, 1024, 2, 1, SOFTWARE);
	st = GetCounterValue(0, &value);
	ASSERT_EQ(st, E_OK);

	/**
	 * [SWS_Os_00531] ⌈ GetCounterValue ()的注意事项：请注意，对于CounterType = HARDWARE的计数器，返回的是实际计时器值（可能已调整的硬件值，请参阅SWS_Os_00384 ），而对于CounterType = SOFTWARE的计数器，则返回当前的“软件”刻度返回值。 ⌋ ( )
	 */


	/**
	 * [SWS_Os_00532] ⌈ GetCounterValue ()的可用性：可用于所有可扩展性类。 ⌋ ( )
	*/
}

TEST(GetElapsedValue)
{
	VAR(TickType, AUTOMATIC) value, elapesd_value;
	VAR(StatusType, AUTOMATIC) st;

#if defined(EXTENDED_STATUS)
	/**
	 * [SWS_Os_00381] ⌈如果	GetElapsedValue ()调用中的输入参数 <CounterID>无效，则GetElapsedValue () 应返回E_OS_ID 。 ⌋ ( )
	 */
	value = 0;
	st = GetElapsedValue(AUTOSAR_NR_COUNTER, &value, &elapesd_value);
	ASSERT_EQ(st, E_OS_ID);

	/**
	 * [SWS_Os_00391] ⌈如果GetElapsedValue ()调用中的 <Value>大于 <CounterID> 的最大允许值， GetElapsedValue () 应返回E_OS_VALUE。 ⌋ ( )
	 */
	DeclareCounter(2, 1024, 2, 1, SOFTWARE);
	value = 1025;
	st = GetElapsedValue(2, &value, &elapesd_value);
	ASSERT_EQ(st, E_OS_VALUE);
#endif

	/**
	 * [SWS_Os_00382] ⌈如果GetElapsedValue ()调用中的输入参数有效， GetElapsedValue()应通过 <ElapsedValue> 返回自给定 <Value> 值以来经过的tick数，并应返回E_OK。 ⌋ ( SRS_Frt_00034 )
	 * [SWS_Os_00460] ⌈ GetElapsedValue () 应在 <Value> 参数中返回
	 * 计数器的当前刻度值。 ⌋ ( )
	 */
	DeclareCounter(2, 1024, 2, 1, SOFTWARE);
	GetCounterValue(2, &value);
	IncrementCounter(2);
	IncrementCounter(2);
	st = GetElapsedValue(2, &value, &elapesd_value);
	ASSERT_EQ(st, E_OK);
	ASSERT_EQ(value, 1);
	ASSERT_EQ(elapesd_value, 1);

	/**
	 * [SWS_Os_00533] ⌈ GetElapsedValue ()注意事项：如果定时器已经通过<Value>值一秒（或多次）时间，返回的结果是错误。原因是服务无法检测到这样的相对溢出。 ⌋ ( )
	 */


	/**
	 * [SWS_Os_00534] ⌈ GetElapsedValue ()的可用性：可用于所有可扩展性类。 ⌋ ( )
	 */
}

TEST_SUIT(Counter)
{
	UTEST_CASE(IncrementCounter);
	UTEST_CASE(GetCounterValue);
	UTEST_CASE(GetElapsedValue);
}
