/*
 * PhotonRTOS础光实时操作系统 -- misc测试
 *
 * Copyright (C) 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include "../utest.h"
#include <Os.h>

TEST(GetActiveApplicationMode)
{
	AppModeType mode;

	mode = GetActiveApplicationMode();
	ASSERT_EQ(mode, OSDEFAULTAPPMODE);
}

TEST_SUIT(Misc)
{
	UTEST_CASE(GetActiveApplicationMode);
}
