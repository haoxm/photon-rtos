/*
 * PhotonRTOS础光实时操作系统 -- Alarm测试
 *
 * Copyright (C) 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include "../utest.h"

ALARMCALLBACK(Alarm1)
{
	static VAR(int32_t, AUTOMATIC) first = 1;

	if (first) {
		pr_debug("        Alarm1 occur\n");
	}
}

ALARMCALLBACK(Alarm2)
{
	static VAR(int32_t, AUTOMATIC) first = 1;

	if (first) {
		pr_debug("        Alarm2 occur\n");
	}
}

ALARMCALLBACK(Alarm3)
{
	static VAR(int32_t, AUTOMATIC) first = 1;

	if (first) {
		pr_debug("        Alarm3 occur\n");
	}
}

TEST(SetRelAlarm)
{
	VAR(StatusType, AUTOMATIC) st;

	st = SetRelAlarm(0, 100, 0);
	ASSERT_EQ(st, E_OK);

	CancelAlarm(0);
}

TEST(SetAbsAlarm)
{
	VAR(StatusType, AUTOMATIC) st;

	st = SetAbsAlarm(1, 500, 0);
	ASSERT_EQ(st, E_OK);

	CancelAlarm(1);
}

TEST(GetAlarmBase)
{
	VAR(StatusType, AUTOMATIC) st;
	VAR(AlarmBaseType, AUTOMATIC) info;

	/* 获取基础信息 */
	st = GetAlarmBase(0, &info);
	ASSERT_EQ(st, E_OK);
	LOGI("alram0: maxallowedvalue:%d, ticksperbase:%d, mincycle:%d",
		info.maxallowedvalue, info.ticksperbase, info.mincycle);
}

TEST(GetAlarm)
{
	VAR(TickType, AUTOMATIC) value;
	VAR(StatusType, AUTOMATIC) st;

	/* 获取tick值 */
	st = SetAbsAlarm(0, 1, 0);
	ASSERT_EQ(st, E_OK);
	st = GetAlarm(0, &value);
	ASSERT_EQ(st, E_OK);
	LOGI("alram0: value:%d", value);

	CancelAlarm(0);
}

TEST(Alarm_)
{
	VAR(TickType, AUTOMATIC) i, j;
	VAR(TickType, AUTOMATIC) value;
	VAR(StatusType, AUTOMATIC) st;
	VAR(AlarmBaseType, AUTOMATIC) base;

	autosar_counters[AUTOSAR_NR_COUNTER - 1].core = GetCoreID();

	for (i = 1; i <= 20; i++) {
		pr_info("%d:\n", i);
		for (j = 0; j < 16; j++) {
			st = IncrementCounter((AUTOSAR_NR_COUNTER - 1));
			if (st != 0) {
				printk("%s\n", Status2String(st));
			}
		}
		switch (i) {
		case 1:
			st = SetAbsAlarm(0, 2, 2);
			pr_debug("   %d SetAbsAlarm(0, 2, 2)\n", st);
			st = SetRelAlarm(1, 3, 3);
			pr_debug("   %d SetRelAlarm(1, 3, 3)\n", st);
			st = SetRelAlarm(2, 4, 0);
			pr_debug("   %d SetRelAlarm(2, 4, 0)\n", st);
			st = GetAlarmBase(0, &base);
			pr_debug("   %d GetAlarmBase(0, &base) max%d, per%d, min%d\n",
				st, base.maxallowedvalue, base.ticksperbase, base.mincycle);
			break;

		case 2:
			st = GetAlarm(1, &value);
			pr_debug("   %d GetAlarm(1, &value) value%d\n", st, value);
			break;

		case 15:
			st = CancelAlarm(0);
			pr_debug("   %d CancelAlarm(0)\n", st);
			st = SetAbsAlarm(2, 16, 0);
			pr_debug("   %d SetAbsAlarm(2, 16, 0)\n", st);
			break;

		default:
			break;
		}
	}
	ASSERT_EQ(0, 1);


}

TEST_SUIT(Alarm)
{
	UTEST_CASE(SetRelAlarm);
	UTEST_CASE(SetAbsAlarm);
	UTEST_CASE(GetAlarmBase);
	UTEST_CASE(GetAlarm);
	/* 测试计数器驱动闹钟时使用 */
	/* UTEST_CASE(Alarm_); */
}
