/*
 * PhotonRTOS础光实时操作系统 -- 测试框架
 *
 * Copyright (C) 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef _UAPI_UTEST_H
#define _UAPI_UTEST_H

#include <Os_internel.h>

#define UTEST_START()							\
	printk("[==========] 开始单元测试\n")

#define UTEST_END()							\
	printk("[==========] 结束单元测试\n")

#define TEST_SUIT(name)							\
	void test_suit_##name(void);					\
	void test_suit_##name(void)

#define TEST(name)							\
	void test_case_##name(void);					\
	void test_case_##name(void)

#define UTEST_SUITE(func)						\
	do {								\
		printk("[----------] 开始测试用例：{ %s }\n", #func);	\
		test_suit_##func();					\
		printk("[----------] 结束测试用例：{ %s }\n", #func);	\
	} while (0)

#define UTEST_CASE(func)						\
	do {								\
		printk("[ RUN      ] 执行单元：[ %s ]\n", #func);	\
		test_case_##func();					\
	} while (0)

#define LOGI(fmt, ...)							\
	printk("[   INFO   ] " fmt "\n", ##__VA_ARGS__)

static inline void __utest_assert(int32_t value, const int8_t *file, int32_t line,
	const int8_t *func, const int8_t *msg, int32_t die_action)
{
	if (value) {
		printk("[       OK ] %s:%d\n", func, line);
	} else {
		printk("[  FAILED  ] 单元：%s 文件:%s 行:%d 信息:%s\n",
			func, file, line, msg);
		if (die_action) {
			panic("[  PANIC   ] 单元测试不通过！请检查代码！\n");
		}
	}
}

#define utest_assert(value, msg) __utest_assert(value,			\
	__FILE__, __LINE__, __func__, msg, 1)
#define utest_except(value, msg) __utest_assert(value,			\
	__FILE__, __LINE__, __func__, msg, 0)

#define ASSERT_TRUE(value)   utest_assert(value, "(" #value ") 是 false")
#define ASSERT_FALSE(value)  utest_assert(!(value), "(" #value ") 是 true")

#define ASSERT_EQ(a, b)  utest_assert((a) == (b), "(" #a ") 不等于 (" #b ")")
#define ASSERT_NE(a, b)  utest_assert((a) != (b),  "(" #a ") 等于 (" #b ")")

#define EXPECT_TRUE(value)   utest_except(value, "(" #value ") 是 false")
#define EXPECT_FALSE(value)  utest_except(!(value), "(" #value ") 是 true")

#define EXPECT_EQ(a, b)  utest_except((a) == (b), "(" #a ") 不等于 (" #b ")")
#define EXPECT_NE(a, b)  utest_except((a) != (b),  "(" #a ") 等于 (" #b ")")

#endif
