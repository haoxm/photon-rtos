/*
 * 础光实时操作系统PhotonRTOS -- 时间功能
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/string.h>
#include <photon/time.h>
#include <photon/irqflags.h>
#include <photon/printk.h>
#include <photon/bug_run.h>
#include <photon/timex.h>

uint64_t jiffies_64 = 0;

void add_jiffies_64(uintptr_t ticks)
{
	uintptr_t flags;

	local_irq_save(flags);
	jiffies_64 += ticks;
	local_irq_restore(flags);
}

uint64_t get_jiffies_64(void)
{
	return jiffies_64;
}

uintptr_t cycles_to_usecs(const uintptr_t cycles)
{
	return arch_cycles_to_usecs(cycles);
}

/**
 * 当前CPU主频大都在3-4GHz，这里假设为5GHz
 * 为了这里能有报错机制，而不是死循环，
 * 以5GHz为基准，设定一个常量
 * 即：在5GHz的CPU运行一秒的时间内，若
 * get_jiffies_64的值仍未增长，则认为
 * 时钟控制器失效。
 * 由于时钟周期和指令周期的区别，以及while循环中
 * get_jiffies_64本身的执行时间，这将是一个相当
 * 长的时间范围。
 * 5*10^9
 */
# define MAX_CPU_HZ 5000000000

void check_timer_validity(void)
{
	uint32_64_t count = get_jiffies_64();
	uint64_t var = MAX_CPU_HZ;
	pr_info("时钟控制器时效性检查开始...\n");
	
	while (get_jiffies_64() - count < 1 && var--){
	}
	if (time_after((uint32_64_t)get_jiffies_64(), count)) {
		pr_info("时钟控制器时效性检查通过\n");
	} else {
		pr_err("定时器设备存在错误，请检查系统！\n");
		BUG();
	}
}
