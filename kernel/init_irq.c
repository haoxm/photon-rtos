/*
 * 础光实时操作系统PhotonRTOS -- 中断初始化
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 * 作者: Yili Zhang <s-zhangyili@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/irq.h>
#include <photon/sched.h>
#include <photon/errno.h>
#include <asm/isr1.h>
/**
 * 注册ISR
 */
static int32_t register_isr_handle(uint32_t irq_id, const struct irq_desc *desc)
{
	if(!desc || !desc->handler ) {
		return -EINVAL;
	}

	if (desc->hw_irq >= irq_count) {
		return -EINVAL;
	}

	/* 设置中断类型 */
	if ((desc->irqflags & HWIRQ_TRIGGER_TYPE_MASK) != HWIRQ_TYPE_NONE ) {
		set_irq_trigger_type(desc->hw_irq, desc->irqflags & HWIRQ_TRIGGER_TYPE_MASK);
	}
	set_irq_priority(desc->hw_irq, desc->pri);
	unmask_irq(desc->hw_irq);

	return 0;
}
/**
 * 启用一类中断
 * 需要将一类中断设置为最高特权级
*/
static void register_irq_handle(uint32_t irq_id)
{
	if (isr1[irq_id]) {
		set_irq_priority(irq_id, 0);
		unmask_irq(irq_id);
	}
}

int32_t init_interrupt(void)
{
	uint32_t i;
	int32_t ret = 0;
	for (i = 0U; i < NR_IRQS; i++) {
		register_irq_handle(i);
		if ((os_irq_desc[i] != NULL)
				&& (os_irq_desc[i]->name != NULL)) {
			ret = register_isr_handle(i, os_irq_desc[i]);
			if (ret != 0) {
				return ret;
			}
		}
	}
	return 0;
}
