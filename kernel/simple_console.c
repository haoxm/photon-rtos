/*
 * 础光实时操作系统PhotonRTOS -- 简单的控制台，只支持输出
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/init.h>

#include <photon/console.h>
#include <photon/printk.h>

extern struct simple_console global_simple_console;

int32_t init_simple_console(void)
{
	global_simple_console.init();
	return 0;
}
