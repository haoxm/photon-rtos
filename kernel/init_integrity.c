/*
 * 础光实时操作系统PhotonRTOS -- 初始化完整性检查
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/init_integrity.h>

uint64_t inited_module;

void init_module(uint8_t module)
{
	inited_module |= (1UL << module);
}

uint8_t all_module_inited(void)
{
	uint8_t i, ret = 0;
	uint64_t tmp = 0;

	for (i = 0; i < MODULE_NUM; i++) {
		tmp |= (1UL << i);
	}

	if (tmp == inited_module) {
		ret = 1;
	}

	return ret;
}

